from instaloader import Instaloader,Profile
import os
from dotenv import load_dotenv

load_dotenv()

L = Instaloader()

# NOTE: rename .env.template to .env, then edit these variables in .env
USER = os.getenv('INSTAGRAM_USER')
print(USER)

 #NOTE: make sure you have logged into instagram in firefox, DO NOT USE UR MAIN ACCOUNT AS YOU COULD GET BANNED
L.load_session_from_file(USER)       

# the profile you want to download from
PROFILE = "relationships.usa"
DOWNLOAD_AMOUNT = 1

class Main:
    def __init__(self,target_profile:str,download_amount:int,username:str=None):
        self.username = username
        self.target_profile = target_profile
        self.download_amount= download_amount
    def run(self):

        profile = Profile.from_username(L.context,self.target_profile)

        posts_sorted_by_likes = sorted(profile.get_posts(),key=lambda post: post.likes, reverse = True)
        
        # NOTE: comment out thhe block of code below if you want to download videos/reels only

        # def post_is_video(post):
        #     if post.is_video == True:
        #         return post
        # posts_sorted_by_likes = list(map(post_is_video,posts_sorted_by_likes))


        for post in range(self.download_amount):
            L.download_post(posts_sorted_by_likes[post],PROFILE)

            

x= Main(target_profile=PROFILE,download_amount=DOWNLOAD_AMOUNT,username=USER)
x.run()